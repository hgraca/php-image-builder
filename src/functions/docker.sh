#!/usr/bin/env bash

docker.login_to_registry(){
  local USER_NAME='' TOKEN='' REGISTRY=''
  local "${@}"

  docker login -u "${USER_NAME}" -p "${TOKEN}" "${REGISTRY}"
}

docker.registry_has_image() {
  IMAGE=${1}
  docker manifest inspect "${IMAGE}" > /dev/null 2>&1
  return $?
}

docker.remove_tag_from_image_name() {
  local IMAGE_NAME=''
  local "${@}"

  # shellcheck disable=SC2001
  echo "${IMAGE_NAME}" | sed 's|\(.*\):.*|\1|'
}
