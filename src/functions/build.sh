#!/usr/bin/env bash

image_name_to_artifact_name(){
  local IMAGE_NAME=''
  local "$@"

  echo ${IMAGE_NAME} | tr '[:lower:]' '[:upper:]' | tr '[\-.]' '_'
}

put_image_version_to_artifact(){
  local ARTIFACT_OUT_PATH='' IMAGE_NAME='' VERSION=''
  local "$@"

  mkdir -p ${ARTIFACT_OUT_PATH}

  echo ${VERSION} > "${ARTIFACT_OUT_PATH}/$(image_name_to_artifact_name IMAGE_NAME="${IMAGE_NAME}")"
}

get_image_version_from_artifact(){
  local ARTIFACT_OUT_PATH='' IMAGE_NAME=''
  local "$@"

  ARTIFACT_NAME="$(image_name_to_artifact_name IMAGE_NAME="${IMAGE_NAME}")"
  BASE_IMAGE_VERSION_PATH="${ARTIFACT_OUT_PATH}/${ARTIFACT_NAME}"

  if [ -f ${BASE_IMAGE_VERSION_PATH} ];
  then
    PARENT_IMAGE_VERSION="$(head -1 "${BASE_IMAGE_VERSION_PATH}")"
  else
    PARENT_IMAGE_VERSION="${!ARTIFACT_NAME}"
  fi

  echo "${PARENT_IMAGE_VERSION}"
}

get_tag(){
  local IMAGE_NAME='' VERSION='latest'
  local "${@}"

  echo "${REGISTRY_IMAGE}/${IMAGE_NAME}:${VERSION}"
}

get_image_path(){
  local IMAGE_NAME=''
  local "${@}"

  echo "images/${IMAGE_NAME}"
}

get_registry_url(){
  echo "https://${REGISTRY_IMAGE}"
}

build() {
  local PARENT_IMAGE_WITH_TAG='' NEW_IMAGE_NAME_WITH_TAG='' DOCKERFILE_PATH='' TARGET_STAGE=''
  local "${@}"

  if [ "${TARGET_STAGE}" != "" ]; then TARGET_STAGE="--target ${TARGET_STAGE}"; fi
  if [ "${PARENT_IMAGE_WITH_TAG}" != "" ]; then PARENT_IMAGE_WITH_TAG="--build-arg PARENT_IMAGE=${PARENT_IMAGE_WITH_TAG}"; fi

  docker build -f "${DOCKERFILE_PATH}" -t "${NEW_IMAGE_NAME_WITH_TAG}" ${PARENT_IMAGE_WITH_TAG} ${TARGET_STAGE} .

  return "$?"
}

#
# PARENT_IMAGE_WITH_ENV = [{PATH}/prod, {PATH}/ci, {PATH}/dev]
# NEW_IMAGE_WITH_ENV = [{PATH}/prod, {PATH}/ci, {PATH}/dev]
#
build_to_repo() {
  local PARENT_IMAGE_NAME_WITH_ENV='' NEW_IMAGE_NAME_WITH_ENV='' DOCKERFILE_PATH='' TARGET_STAGE='' NEW_IMAGE_VERSION=''
  local "${@}"

  if [ "${PARENT_IMAGE_NAME_WITH_ENV}" != "" ]; then
    if docker.registry_has_image "${PARENT_IMAGE_NAME_WITH_ENV}-${NEW_IMAGE_VERSION}"; then
      PARENT_IMAGE_WITH_TAG="${PARENT_IMAGE_NAME_WITH_ENV}-${NEW_IMAGE_VERSION}"
    else
      PARENT_IMAGE_WITH_TAG="${PARENT_IMAGE_NAME_WITH_ENV}-latest"
    fi
  fi

  NEW_IMAGE_NAME_WITH_TAG="${NEW_IMAGE_NAME_WITH_ENV}-${NEW_IMAGE_VERSION}"

  # It should be allowed to fail, as when it's a first run the image doesn't exist yet
  docker pull "${NEW_IMAGE_NAME_WITH_ENV}-latest" > /dev/null 2>&1 || true       # Get the latest so we maybe reuse some layers
  docker pull "${NEW_IMAGE_NAME_WITH_TAG}" > /dev/null 2>&1 || true  # If the image already exists, pull it so we save time building it

  # Build the new image
  build PARENT_IMAGE_WITH_TAG="${PARENT_IMAGE_WITH_TAG}" \
    NEW_IMAGE_NAME_WITH_TAG="${NEW_IMAGE_NAME_WITH_TAG}" \
    DOCKERFILE_PATH="${DOCKERFILE_PATH}" \
    TARGET_STAGE="${TARGET_STAGE}"
  CODE="$?"; if [ "$CODE" != "0" ]; then return $CODE; fi # terminate if the previous step failed

  # Push the new image, so it is available to other gitlab-runner servers
  docker push "${NEW_IMAGE_NAME_WITH_TAG}"
  CODE="$?"; if [ "$CODE" != "0" ]; then return $CODE; fi # terminate if the previous step failed

  echo "Pushed new image '${NEW_IMAGE_NAME_WITH_TAG}', based on parent '${PARENT_IMAGE_WITH_TAG}'."
}

tag_to_repo() {
  local IMAGE_NAME_WITH_TAG='' NEW_TAG=''
  local "${@}"

  # When we add the tag, the image should already have been built and pushed to the repository,
  # but it might not be in the current server, so we need to pull
  docker pull "${IMAGE_NAME_WITH_TAG}"
  CODE="$?"; if [ "$CODE" != "0" ]; then return $CODE; fi # terminate if the previous step failed

  NEW_IMAGE_NAME_WITH_TAG="$(docker.remove_tag_from_image_name IMAGE_NAME="${IMAGE_NAME_WITH_TAG}"):${NEW_TAG}"

  # Tag the image and push the new tag
  docker tag "${IMAGE_NAME_WITH_TAG}" "${NEW_IMAGE_NAME_WITH_TAG}"
  CODE="$?"; if [ "$CODE" != "0" ]; then return $CODE; fi # terminate if the previous step failed
  docker push "${NEW_IMAGE_NAME_WITH_TAG}"
  CODE="$?"; if [ "$CODE" != "0" ]; then return $CODE; fi # terminate if the previous step failed
}
