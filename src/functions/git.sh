#!/usr/bin/env bash

git.get_last_commit_short_sha(){
  git rev-parse --short HEAD
}
