#!/usr/bin/env bash

ACTION=${1:-'build'}
ARGS=( "${@}" )
unset "ARGS[0]"
RETURN_CODE=0
export DOCKER_BUILDKIT=1

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
ROOT_PATH="$(realpath "${SCRIPT_DIR}/..")"
VENDOR_PATH="${ROOT_PATH}/vendor"

. "${VENDOR_PATH}/autoload.sh"

#=======================================================================================================================
# FUNCTIONS
#=======================================================================================================================

help() {
  echo "Usage: ${0} <ACTION> [-h]"
  echo "Actions:"
  echo ""
  echo "         ${0} help"
  echo ""
  echo "         ${0} login \
                   REGISTRY_USER='hgadmin' \
                   REGISTRY_TOKEN='giBerIshtoKen'"
  echo ""
  echo "         ${0} build \ # Builds an image locally
                   IMAGE_NAME='my-image' \
                   VERSION='1.0.0' \
                   TARGET_STAGE='my-stage' # Optional."
  echo ""
  echo "         ${0} build_to_repo \ # Builds an image locally and pushes it to the remote repository
                   IMAGE_NAME='my-image' \
                   VERSION='1.0.0' \
                   TARGET_STAGE='my-stage' # Optional."
}

validate_image_and_version_are_provided() {
  # Both the IMAGE_NAME and VERSION must not be empty. It might occur, specially if using scheduled pipelines.
  if [ "${IMAGE_NAME}" == "" ] || [ "${VERSION}" == "" ]; then
    echo "[ERROR] Both the IMAGE_NAME and the VERSION must be set."
    return 1
  fi
}

validate_image_does_not_exist_yet() {
  NEW_IMAGE="$(get_tag IMAGE_NAME="${IMAGE_NAME}" VERSION="${VERSION}")"
  # If this version already exists, we must not overwrite it
  if docker manifest inspect "${NEW_IMAGE}" > /dev/null 2>&1; then
    echo "[ERROR] This image already exists in the remote repository! If you want to replace it, please remove it first."
    return 1
  fi
}

#=======================================================================================================================
# MAIN
#=======================================================================================================================

case ${ACTION} in
  # --------------------------------------------------------------------------------------------------------------------
  'help')
    help
    ;;
  # --------------------------------------------------------------------------------------------------------------------
  'login')
    USER_NAME='' TOKEN='' REGISTRY=''
    eval "${ARGS[@]}"

    docker.login_to_registry USER_NAME="${USER_NAME}" TOKEN="${TOKEN}" REGISTRY="${REGISTRY}"
    RETURN_CODE="$?"; if [ "$RETURN_CODE" != "0" ]; then exit $RETURN_CODE; fi # terminate if the previous step failed
    ;;
  # --------------------------------------------------------------------------------------------------------------------
  'build_to_local')
    PARENT_IMAGE_WITH_VERSION='' NEW_IMAGE_WITH_ENV='' DOCKERFILE_PATH='' TARGET_STAGE=''
    eval "${ARGS[@]}"

    if [ "${NEW_IMAGE_WITH_ENV}" == "" ] || [ "${DOCKERFILE_PATH}" == "" ]; then
      echo "[ERROR] Both the IMAGE_NAME and the VERSION must be set."
      exit 1
    fi

    NEW_IMAGE_VERSION="$(git.get_last_commit_short_sha)"
    NEW_IMAGE_WITH_VERSION="${NEW_IMAGE_WITH_ENV}-${NEW_IMAGE_VERSION}"

    echo "Building image '${NEW_IMAGE_WITH_ENV}' with version '${NEW_IMAGE_VERSION}' from parent '${PARENT_IMAGE_WITH_VERSION}' (stage='${TARGET_STAGE}')..."

    build \
      PARENT_IMAGE_WITH_VERSION="${PARENT_IMAGE_WITH_VERSION}" \
      NEW_IMAGE_WITH_VERSION="${NEW_IMAGE_WITH_VERSION}" \
      DOCKERFILE_PATH="${DOCKERFILE_PATH}" \
      TARGET_STAGE="${TARGET_STAGE}"
    RETURN_CODE="$?"; if [ "$RETURN_CODE" != "0" ]; then exit $RETURN_CODE; fi # terminate if the previous step failed
    echo "New image '${NEW_IMAGE_WITH_VERSION}' built!"

    docker tag "${NEW_IMAGE_WITH_VERSION}" "${NEW_IMAGE_WITH_ENV}-latest"
    echo "New image '${NEW_IMAGE_WITH_VERSION}' tagged '${NEW_IMAGE_WITH_ENV}-latest'!"
    ;;
  # --------------------------------------------------------------------------------------------------------------------
  'build_to_repo')
    PARENT_IMAGE_NAME_WITH_ENV='' NEW_IMAGE_NAME_WITH_ENV='' DOCKERFILE_PATH='' TARGET_STAGE='' NEW_IMAGE_VERSION=''
    eval "${ARGS[@]}"

    if [ "${NEW_IMAGE_NAME_WITH_ENV}" == "" ] || [ "${DOCKERFILE_PATH}" == "" ] || [ "${NEW_IMAGE_VERSION}" == "" ]; then
      echo "[ERROR] The NEW_IMAGE_NAME_WITH_ENV, NEW_IMAGE_VERSION and DOCKERFILE_PATH must be provided."
      exit 1
    fi

    echo "Building image '${NEW_IMAGE_NAME_WITH_ENV}-${NEW_IMAGE_VERSION}' and pushing to registry..."

    build_to_repo \
      PARENT_IMAGE_NAME_WITH_ENV="${PARENT_IMAGE_NAME_WITH_ENV}" \
      NEW_IMAGE_NAME_WITH_ENV="${NEW_IMAGE_NAME_WITH_ENV}" \
      DOCKERFILE_PATH="${DOCKERFILE_PATH}" \
      NEW_IMAGE_VERSION="${NEW_IMAGE_VERSION}" \
      TARGET_STAGE="${TARGET_STAGE}"
    RETURN_CODE="$?"; if [ "$RETURN_CODE" != "0" ]; then exit $RETURN_CODE; fi # terminate if the previous step failed

    echo "New image built and ready for use!"
    ;;
  # --------------------------------------------------------------------------------------------------------------------
  'tag_to_repo')
    IMAGE_NAME_WITH_TAG='' NEW_TAG=''
    eval "${ARGS[@]}"

    if [ "${IMAGE_NAME_WITH_TAG}" == "" ] || [ "${NEW_TAG}" == "" ]; then
      echo "[ERROR] The IMAGE_NAME_WITH_TAG and NEW_TAG must be set."
      exit 1
    fi

    if [ "${NEW_TAG}" != 'latest' ] && docker.registry_has_image "${NEW_TAG}"; then
      echo "[ERROR] This image tag '${NEW_TAG}' already exists in the remote repository! If you want to replace it, please remove it first."
      exit 2
    fi

    echo "Tagging image '${IMAGE_NAME_WITH_TAG}' as '${NEW_TAG}' and pushing tag to the remote repo..."
    tag_to_repo \
      IMAGE_NAME_WITH_TAG="${IMAGE_NAME_WITH_TAG}" \
      NEW_TAG="${NEW_TAG}"
    RETURN_CODE="$?"; if [ "$RETURN_CODE" != "0" ]; then exit $RETURN_CODE; fi # terminate if the previous step failed

    echo "Tagged image '${IMAGE}' as '${NEW_TAG}'."
    ;;
  # --------------------------------------------------------------------------------------------------------------------
  *)
    echo "Invalid action: ${ACTION}"
    help
    exit 4
    ;;
  # --------------------------------------------------------------------------------------------------------------------
esac

echo ''
echo ''

exit $RETURN_CODE
