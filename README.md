# PHP image builder

A repository to build hierarchical (ie: prod->ci->dev) docker images and publish them to docker hub.

It is capable of building images for any project, both inside and outside a GitLab pipeline.

The image name format is `<username>/<image_name>:<env>-<version>`

## How it works

1. When we make changes to an image, we open an MR and the new image will be built with a new version, which is the last commit short sha.
   You can then test that new image in your project.
2. Once you merge that MR, a new pipeline will tag that image with `latest`.
   You should, however, not use the `latest` tag in your project, as a new version might suddenly break the MR
   pipeline of your project or even the application itself after deployment. We tag it as latest because it is
   useful in some cases.

## How to add a new image

1. Create a folder for the new image in `images/`. Use the following pattern for the folder name: `<project-name>-<root_base_image>.<environment>`.

   For example:
    - `full-php-8.2-fpm-alpine.prod`
    - `full-php-8.2-fpm-alpine.ci`
    - `full-php-8.2-fpm-alpine.dev`

2. In that folder, place everything related to that image.
3. In `.gitlab/stages/010-build-prod.yml`, create a new concrete job to build the prod image, on a merge request.
4. In `.gitlab/stages/015-tag-prod.yml`, create a new concrete job to tage the prod image as latest, after merging.
5. In `.gitlab/stages/010-build-ci.yml`, create a new concrete job to build the ci image, on a merge request.
6. In `.gitlab/stages/015-tag-ci.yml`, create a new concrete job to tage the ci image as latest, after merging.
7. In `.gitlab/stages/010-build-dev.yml`, create a new concrete job to build the dev image, on a merge request.
8. In `.gitlab/stages/015-tag-dev.yml`, create a new concrete job to tage the dev image as latest, after merging.

## Using this repository in a development environment

You can build images locally by using the composer commands

At this moment, these are the commands available:
```
composer build-full-php-8.2-fpm-alpine:prod
composer build-full-php-8.2-fpm-alpine:ci
composer build-full-php-8.2-fpm-alpine:dev
```
