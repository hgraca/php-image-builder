#!/usr/bin/env sh

#
# UNOPTIMIZED AUTOLOADER
#
#
# NOTE: This is an unoptimized autoloader, as it needs to search for the files to include.
#       This is useful during development, in case we move the repository to a different location.
#       Please use the flag `--optimize-autoload` a production ready auto-loader, where the file
#       paths to source will be hardcoded here.
#
if [ "${VENDOR_PATH}" = "" ]; then
  echo "ERROR: you must define the variable VENDOR_PATH before sourcing the autoloader."
  exit 1
fi
PROJECT_NAME="hgraca/php-image-builder"
ROOT_PATH="$(realpath "${VENDOR_PATH}/..")"

set -- 'hgraca/php-image-builder/src'

for PROJECT_SRC_FOLDER in "$@"; do
  if [ "${PROJECT_SRC_FOLDER#"${PROJECT_NAME}"}" != "$PROJECT_SRC_FOLDER" ]; then
    ABS_SRC_FOLDER="${ROOT_PATH}/${PROJECT_SRC_FOLDER#"${PROJECT_NAME}/"}"
  else
    ABS_SRC_FOLDER="${VENDOR_PATH}/${PROJECT_SRC_FOLDER}"
  fi

  # @see https://github.com/koalaman/shellcheck/wiki/SC2044#for-posix
  find "${ABS_SRC_FOLDER}" -type f -name "*.sh" ! -name "$(printf "*\n*")" > TMP
  while IFS= read -r FILE
  do
    . "${FILE}"
  done < TMP
  rm TMP
done
