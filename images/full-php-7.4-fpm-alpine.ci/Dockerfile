ARG PARENT_IMAGE
FROM ${PARENT_IMAGE}

MAINTAINER Herberto Graca <herberto.graca@gmail.com>

ARG UID="1000"
ARG GID="1000"

RUN curl -s https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer \
    && composer self-update

# install mysql pdo extension
RUN docker-php-ext-install pdo_mysql

# install postgresql pdo extension
RUN apk --update add --no-cache postgresql-dev \
    && docker-php-ext-install pdo_pgsql \
    && rm -rf /var/cache/apk/*

# This is needed for the mssql pdo extension
RUN apk --update add --no-cache --virtual .build-deps linux-headers $PHPIZE_DEPS autoconf g++ make \
    && apk --update add icu-dev \
    && mkdir -p /tmp/pear/cache \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl \
    && docker-php-ext-enable intl \
    && apk del .build-deps \
    && rm -rf /var/cache/apk/* \
    && pecl clear-cache

# install mssql pdo extension
RUN apk --update add --no-cache --virtual .phpize-deps $PHPIZE_DEPS freetds-dev unixodbc-dev autoconf g++ make \
    && apk --update add --no-cache unixodbc \
    && pecl install sqlsrv-5.10.1 pdo_sqlsrv-5.10.1 \
    && docker-php-ext-enable sqlsrv pdo_sqlsrv \
    && apk del .phpize-deps \
    && rm -rf /tmp/pear \
    && rm -rf /var/cache/apk/*

RUN apk --update add --no-cache --virtual .build-deps linux-headers $PHPIZE_DEPS autoconf g++ make \
    && mkdir -p /tmp/pear/cache \
    && docker-php-ext-configure bcmath \
    && docker-php-ext-install bcmath \
    && apk del .build-deps \
    && rm -rf /var/cache/apk/* \
    && pecl clear-cache

RUN apk --update add --no-cache --virtual .build-deps linux-headers $PHPIZE_DEPS autoconf g++ make \
    && mkdir -p /tmp/pear/cache \
    && docker-php-ext-configure ctype \
    && docker-php-ext-install ctype \
    && apk del .build-deps \
    && rm -rf /var/cache/apk/* \
    && pecl clear-cache

RUN apk --update add --no-cache --virtual .build-deps linux-headers $PHPIZE_DEPS autoconf g++ make \
    && mkdir -p /tmp/pear/cache \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl \
    && apk del .build-deps \
    && rm -rf /var/cache/apk/* \
    && pecl clear-cache

RUN apk --update add --no-cache --virtual .build-deps linux-headers $PHPIZE_DEPS autoconf g++ make \
    && apk add --no-cache libzip-dev unzip \
    && mkdir -p /tmp/pear/cache \
    && docker-php-ext-configure zip \
    && docker-php-ext-install zip \
    && apk del .build-deps \
    && rm -rf /var/cache/apk/* \
    && pecl clear-cache

RUN apk --update add --no-cache --virtual .build-deps linux-headers $PHPIZE_DEPS autoconf g++ make \
    && apk add --no-cache libxml2-dev \
    && mkdir -p /tmp/pear/cache \
    && docker-php-ext-configure dom \
    && docker-php-ext-install dom \
    && apk del .build-deps \
    && rm -rf /var/cache/apk/* \
    && pecl clear-cache

# pcov is used for running phpunit with coverage, faster
RUN apk --update add --no-cache --virtual .build-deps autoconf g++ make \
    && pecl install -o -f pcov \
    && docker-php-ext-enable pcov \
    && pecl clear-cache \
    && apk del .build-deps \
    && rm -rf /tmp/pear \
    && rm -rf /var/cache/apk/*

# install PHPMetrics
run curl -L "https://github.com/phpmetrics/PhpMetrics/blob/master/releases/phpmetrics.phar?raw=true" -o phpmetrics.phar \
    && chmod +x phpmetrics.phar \
    && mv phpmetrics.phar /usr/local/bin/phpmetrics
